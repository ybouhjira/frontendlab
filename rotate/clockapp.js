function refresh(){
    now = new Date();
    $("#hour_indic").css('transform',"rotate("+(360*(now.getHours() + now.getMinutes()%10*0.1)/12 -90)+"deg)");   
    $("#min_indic").css('transform',"rotate("+(360*now.getMinutes()/60-90)+"deg)");
    $("#second_indic").css('transform',"rotate("+(360*now.getSeconds()/60-90)+"deg)");
}
$(function(){
    refresh();
    setInterval(function(){
        refresh();
    }, 1000);
        
});